package be.multimedi.trench.utilityClasses.inputUtilProto;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class InputUtilProto {
    private static final String INVALID_MSG = "Invalid input! Please try again...";

    /*public static String ask(String message) { //waarom werkt dit niet?
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.println(message);
            return keyboard.nextLine();
        }
    }*/
    public static String ask(String message) { //Yannick

        Scanner keyboard = new Scanner(System.in);
            System.out.println(message);
            return keyboard.nextLine();
    }

    public static int askForInt(String message) { //yannick
        while (true) { //loop
            String input = ask(message); // return van ask
            try {
                return Integer.parseInt(input); //Integer om nextInt te vermijden
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static boolean askYOrN(String message) {
        while (true) {
            String input = ask(message + "(y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'y':
                    return true;
                case 'n':
                    return false;
                default:
                    break;
            }
        }
    }
    public static double askForDouble(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Double.parseDouble(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }
    


}