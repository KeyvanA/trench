package be.multimedi.trench.utilityClasses.weaponUtil;

import be.multimedi.trench.utilityClasses.randomizer.Rand;

public abstract class WeaponUtil {


    public static int karabiner98k() {
        final int MAGSIZE = 5;
        final int BASEDMG = 10;
        return BASEDMG+Rand.randInt(15);
    }
}
