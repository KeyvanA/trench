package be.multimedi.trench.utilityClasses.testUtil;

import java.util.Scanner;

public class TestUtil {
    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in);

    public static String ask(String message) {
        System.out.println(message);
        return KEYBOARD.nextLine();
    }


    public static int askForInt(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }
}
