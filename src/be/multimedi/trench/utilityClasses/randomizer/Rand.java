package be.multimedi.trench.utilityClasses.randomizer;

import java.util.Random;

public abstract class Rand {

    public static int randInt(int range) {
        Random DiceRoll = new Random();
        return DiceRoll.nextInt(range);
    }
}
