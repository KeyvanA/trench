package be.multimedi.trench.utilityClasses.outputUtilProto;

public abstract class OutputUtilProto {

    public static void printMainTitle(String text) {
        System.out.println(text);
        for (int i = 0; i < text.length();i++) {
            System.out.print("=");
        }
        System.out.println("\n");
    }

    public static void printTitle(String text) {
        System.out.println(text);
        for (int i = 0; i < text.length();i++) {
            System.out.print("-");
        }
        System.out.println("\n");
    }

    public static void printText(String text) {
        System.out.println(text);
    }

    public static void printTextUpperCase(String text) {
        System.out.println(text.toUpperCase());
    }

}
