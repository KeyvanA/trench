package be.multimedi.trench.trooper;

import be.multimedi.trench.utilityClasses.inputUtilProto.InputUtilProto;
import be.multimedi.trench.utilityClasses.outputUtilProto.OutputUtilProto;
import be.multimedi.trench.utilityClasses.randomizer.Rand;
import be.multimedi.trench.utilityClasses.weaponUtil.WeaponUtil;

public class CharacterCreation {

    public static void createCharacter(){
        String name = InputUtilProto.ask("What's your name?:");

        Hero hero = new Hero(name,"Car98", Rand.randInt(50)+50
                ,Rand.randInt(50),Rand.randInt(50)+50);

        System.out.println(hero.toString());

    }

    public static void createEnemy() {
        String name = "Murdoc";

        EnemyTrooper ET = new EnemyTrooper(name, "Car98", Rand.randInt(50) + 50
                , Rand.randInt(50), Rand.randInt(50) + 50);



        System.out.println(ET.toString());

    }
    public static void createStrawman() {
        String name = "Scarecrow";

        EnemyTrooper ET = new EnemyTrooper(name, "unarmed", 100
                , 0, 0);



        System.out.println(ET.toString());

    }


}
