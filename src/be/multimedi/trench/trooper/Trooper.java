package be.multimedi.trench.trooper;

abstract public class Trooper {
    String name;
    String weapon;
    int Health;
    int armour;
    int ballisticSkill;

    //int initiative;
    //int dodge;

    //****** Constructor *************************************************************

    public Trooper(String name, String weapon, int health, int armour, int ballisticSkill) {
        this.name = name;
        this.weapon = weapon;
        Health = health;
        this.armour = armour;
        this.ballisticSkill = ballisticSkill;
    }
    //****** Get/Set *****************************************************************

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return Health;
    }

    public void setHealth(int health) {
        Health = health;
    }

    public int getArmour() {
        return armour;
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public int getBallisticSkill() {
        return ballisticSkill;
    }

    public void setBallisticSkill(int ballisticSkill) {
        this.ballisticSkill = ballisticSkill;
    }
    //****** ToString *****************************************************************
    @Override
    public String toString() {
        return "Trooper{" +
                "name='" + name + '\'' +
                ", weapon='" + weapon + '\'' +
                ", Health=" + Health +
                ", armour=" + armour +
                ", ballisticSkill=" + ballisticSkill +
                '}';
    }
}
