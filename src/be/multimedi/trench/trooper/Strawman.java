package be.multimedi.trench.trooper;

public class Strawman extends Trooper {

    public Strawman(String name, String weapon, int health, int armour, int ballisticSkill) {
        super(name, weapon, health, armour, ballisticSkill);
    }
}
