package be.multimedi.trench.trooper;

import be.multimedi.trench.Interfaces.ShootInterface;

public class EnemyTrooper extends Trooper implements ShootInterface {

    public EnemyTrooper(String name, String weapon, int health, int armour, int ballisticSkill) {
        super(name, weapon, health, armour, ballisticSkill);
    }


    @Override
    public int shoot() {
        System.out.println(name + " fires at you!");
        //TODO to hit + to wound system creation
        return 0;
    }
}
