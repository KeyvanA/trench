package be.multimedi.trench.trooper;

import be.multimedi.trench.Interfaces.ShootInterface;

public class Hero extends EnemyTrooper {

    public Hero(String name, String weapon, int health, int armour, int ballisticSkill) {
        super(name, weapon, health, armour, ballisticSkill);
    }

    @Override
    public int shoot() {
        return super.shoot();
    }
}
