package be.multimedi.trench.apps;

import be.multimedi.trench.trooper.CharacterCreation;
import be.multimedi.trench.utilityClasses.inputUtilProto.InputUtilProto;
import be.multimedi.trench.utilityClasses.outputUtilProto.OutputUtilProto;

public class App {
    public static void main(String[] args) {

        OutputUtilProto.printMainTitle("Welcome to the Trench!");
        OutputUtilProto.printText("What do you want to do?");

        OutputUtilProto.printText("1. Character creation");
        OutputUtilProto.printText("2. Target practise");
        OutputUtilProto.printText("?. Quit");

        switch (InputUtilProto.askForInt("Make your choice: ")) {
            case 1: CharacterCreation.createCharacter(); break; //tester for hero
            case 2: CharacterCreation.createCharacter(); CharacterCreation.createStrawman(); break;

        }






    }
}
